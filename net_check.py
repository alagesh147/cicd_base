import requests
import os,socket,sys
import subprocess, platform

def connect_http(url):
#    url = "https://google.com"
  
    http_url = "http://" + url
    https_url = "https://" + url

    try:
       response = requests.get(url=http_url,timeout=(3.05, 2))
       if response.status_code == 200:
        print('Http connection Succesful')
        
       else:
        print ("Error in Http connetction, Error Code : {}".format(response.status_code,))
      
    except:
        try:
          response = requests.get(url=https_url)
          if response.status_code == 200:
           print('Https connection Succesful')
                
          else:
           print ("Error in Https connetction, Error Code : {}".format(response.status_code,))
           
        except:
           print ("Error in Http(s) connetction")
              

   
def check_dns(url):

    
    try:
        response   =  socket.gethostbyname(url)
        print('Dns Lookup Test Succesfull, IP address of the Source:' +  response)
    except:
        print ("Dns Lookup Failed check Name servers")
        
    

def ping_test(url):
    try:
        ping_target = socket.gethostbyname(url)
        #response =  subprocess.check_output("ping -{} 1 {}".format('n' if platform.system().lower()=="windows" else 'c', ping_target), shell=True)
        response =  os.system("ping -c 1 " + ping_target +   "> /dev/null 2>&1")
        
        
        if response == 0:
            pingstatus = "Ping Test to : " + url + " Successfull"
        else:
            pingstatus = "Ping Test to : " + url + " Failed" 
        print (pingstatus)

    except:
        ping_target = "8.8.8.8"
        response =  os.system("ping -c 1 " + ping_target +   "> /dev/null 2>&1")
               
        if response == 0:
            pingstatus = "Ping Test to public server successfull"
        else:
            pingstatus =  "Ping Test to public server unsuccessfull" 
        print (pingstatus)

def main():
    url = "google.com"
    web_check = connect_http(url)
    dns_check = check_dns(url)
    ping_check = ping_test(url)


if __name__ == '__main__':
    # execute only if run as the entry point into the program
    main()            