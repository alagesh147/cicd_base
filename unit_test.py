from io import StringIO 
import unittest 
from unittest.mock import patch 
from net_check import ping_test 
  
class TestPing(unittest.TestCase): 

    def setUp(self): 
       self.result = "Ping Test to : google.com Successfull\n"
    
    def test_ping_with_valid_url(self):
        url = "google.com"
        response = ping_test(url)
        with patch('sys.stdout', new = StringIO()) as fake_out: 
            ping_test(url)
            self.assertEqual(fake_out.getvalue(), self.result)

    def test_ping_without_valid_url(self): 
        url = "google.comm"
        response = ping_test(url)
        with patch('sys.stdout', new = StringIO()) as fake_out: 
            ping_test(url)
            self.assertNotEqual(fake_out.getvalue(), self.result)
        
if __name__ == '__main__':
    unittest.main()